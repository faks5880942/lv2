<!DOCTYPE html>
<html>
<head>
	<title>Drugi zadatak</title>
	<meta charset="utf-8">
</head>
	<h1>Planets data</h1>
	<table border="1px">
	<caption>Data about the planets of our solar system (Planetary facts taken form <a href="https://nssdc.gsfc.nasa.gov/planetary/factsheet/">Nasa's Planetary Fact Sheet-Metric</a>).</caption>
	<thead>
	<tr>
		<th colspan="2"</th>
		<th>Name</th>
		<th>Mass <br>(10<sup>24</sup>kg)</th>
		<th>Diameter <br>(km)</th>
		<th>Density<br>(kg/m<sup>3</sup>)</th>
		<th>Gravity<br>(m/s<sup>2</sup>)</th>
		<th>Lenght of <br>day(hours)</th>
		<th>Distance from <br>Sun(10<sup>6</sup>km)</th>
		<th>Mean <br>temperature <br>(°C)</th>
		<th>Number of<br> moons</th>
		<th>Notes</tr>
	</tr>
	</thead>

	<tbody>
		<tr>
		<td rowspan="4" colspan="2"><b>Terrestial planets</b></td>
		<td><b>Mercury</b></td>
		<td>0.330</td>
		<td>4,879</td>
		<td>5427</td>
		<td>3.7</td>
		<td>4222.6</td>
		<td>57.9</td>
		<td>167</td>
		<td>0</td>
		<td colspan="2">Closest to the Sun</td>
		</tr>

		<tr>
		<td><b>Venus</b</td>
		<td>4.87</td>
		<td>12,104</td>
		<td>5243</td>
		<td>8.9</td>
		<td>2802.0</td>
		<td>108.2</td>
		<td>464</td>
		<td>0</td>
		<td colspan="2"></td>
		</tr>

		<tr>
		<td><b>Earth</b></td>
		<td>5.97</td>
		<td>12,756</td>
		<td>5514</td>
		<td>9.8</td>
		<td>24.0</td>
		<td>149.6</td>
		<td>15</td>
		<td>1</td>
		<td colspan="2">Our world</td>
		</tr>
		
		<tr>
		<td><b>Mars</b></td>
		<td>0.642</td>
		<td>6,792</td>
		<td>3933</td>
		<td>3.7</td>
		<td>24.7</td>
		<td>227.9</td>
		<td>-65</td>
		<td>2</td>
		<td colspan="2">The red planet</td>
		</tr>

	
		<tr>
		<td rowspan="4" ><b>Jovian<br>planets</b></td>
		<td rowspan="2" ><b>Gas giants</b></td>
		<td><b>Jupiter</b></td>
		<td>1898</td>
		<td>142,984</td>
		<td>1326</td>
		<td>23.1</td>
		<td>9.9</td>
		<td>778.6</td>
		<td>-110</td>
		<td>67</td>
		<td colspan="2">The largest planet</td>
		</tr>

		<tr>
		<td><b>Saturn</b></td>
		<td>568</td>
		<td>120,536</td>
		<td>687</td>
		<td>9.0</td>
		<td>10.7</td>
		<td>1433.5</td>
		<td>-140</td>
		<td>62</td>
		<td colspan="2"></td>
		</tr>

		<tr>
		<td rowspan="2" ><b>Ice giants</b></td>
		<td><b>Uranus</b></td>
		<td>86.8</td>
		<td>51,118</td>
		<td>1271</td>
		<td>8.7</td>
		<td>17.2</td>
		<td>2872.5</td>
		<td>-195</td>
		<td>27</td>
		<td colspan="2"></td>
		</tr>

		<tr>
		<td><b>Neptune</b></td>
		<td>102</td>
		<td>49,528</td>
		<td>1638</td>
		<td>11.0</td>
		<td>16.1</td>
		<td>4495.1</td>
		<td>-200</td>
		<td>14</td>
		<td colspan="2"></td>
		</tr>

		<tr>
		<td colspan="2"><b>Dwarf planets</b></td>
		<td><b>Pluto</b></td>
		<td>0.0146</td>
		<td>2,370</td>
		<td>2095</td>
		<td>0.7</td>
		<td>153.3</td>
		<td>5096.4</td>
		<td>-225</td>
		<td>5</td>
		<td colspan="2">Declassified as the planet in <br> 2006, but this remains<br> controversial</td>
		</tr>






	</tbody>
	</table>
 
<body>
	
</body>
</html>

